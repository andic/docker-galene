ARG ARG_NAME=galene
#ARG ARG_VERSION=galene-x.y.z
ARG ARG_VERSION=master
ARG ARG_USER_NAME=${ARG_NAME}
ARG ARG_GROUP_NAME=${ARG_NAME}


## Build Container
FROM golang:1-alpine as builder

ARG ARG_NAME
ARG ARG_VERSION
ENV NAME=${ARG_NAME}
ENV VERSION=${ARG_VERSION}

RUN set -ex \
  && echo "Building ${NAME} - ${VERSION}" \
  && apk update \
  && apk upgrade \
  && apk add \
    git \
  && rm -rf /var/cache/apk/* \
  && cd /opt \
  && git clone --depth=1 --branch ${VERSION} https://github.com/jech/galene \
  && cd galene \
  && CGO_ENABLED=0 go build -ldflags='-s -w' \
  && cd galene-password-generator \
  && CGO_ENABLED=0 go build -ldflags='-s -w' -o galene-password-generator \
  && cd .. \
  && echo "Done building ${NAME} - ${VERSION}"


## Runtime Container
FROM alpine:3.15

LABEL org.opencontainers.image.authors="Andreas Cislik <git@a11k.net>"
LABEL org.opencontainers.image.source="https://gitlab.com/andic/docker-galene"
LABEL org.opencontainers.image.licenses="MIT"
LABEL org.opencontainers.image.title="Galène"
LABEL org.opencontainers.image.description="The Galène videoconference server"

ARG ARG_NAME
ARG ARG_VERSION
ARG ARG_USER_NAME
ARG ARG_GROUP_NAME
ENV NAME=${ARG_NAME}
ENV VERSION=${ARG_VERSION}
ENV USER_NAME=${ARG_USER_NAME}
ENV GROUP_NAME=${ARG_GROUP_NAME}
ENV GALENE_SECRET=long-and-cryptic
ENV LOGS_DIRECTORY=stdout

ENTRYPOINT ["/usr/local/bin/docker-entrypoint.sh"]
VOLUME /opt/${NAME}/data /opt/${NAME}/groups /opt/${NAME}/recordings
EXPOSE 8000

WORKDIR /opt/${NAME}
COPY --from=builder /opt/galene/galene .
COPY --from=builder /opt/galene/galene-password-generator/galene-password-generator .
COPY --from=builder /opt/galene/static static
COPY docker-entrypoint.sh /usr/local/bin/docker-entrypoint.sh
COPY data/passwd /opt/galene/templates/data/passwd
COPY data/ice-servers.json /opt/galene/templates/data/ice-servers.json
COPY groups/public.json /opt/galene/templates/groups/public.json

RUN set -ex \
  && echo "Installing ${NAME} - ${VERSION}" \
  && apk update \
  && apk upgrade \
  && apk add \
    gettext \
    tzdata \
  && rm -rf /var/cache/apk/* \
  && chmod 755 /usr/local/bin/docker-entrypoint.sh \
  && addgroup ${GROUP_NAME} -g 1000 \
  #&& adduser -u 1000 -G ${GROUP_NAME} --home /opt/${NAME} -D ${USER_NAME} \
  && adduser -u 1000 -G ${GROUP_NAME} -D ${USER_NAME} \
  #&& useradd -r -m -d /opt/${NAME} ${USER_NAME} \
  #&& mkdir -p /etc/${NAME} \
  && echo "Done installing ${NAME} - ${VERSION}" \
  
