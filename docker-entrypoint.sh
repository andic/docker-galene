#!/bin/sh

echo "starting docker-entrypoint.sh"
echo "$(date)"
echo "${NAME} - ${VERSION}"


## create config
#if [ ! -e /opt/galene/data/ice-servers.json ]; then
#  cp /opt/galene/templates/data/passwd /opt/galene/data/passwd
#  cp /opt/galene/templates/data/ice-servers.json /opt/galene/data/ice-servers.json
#  cp /opt/galene/templates/groups/public.json /opt/galene/groups/public.json
#fi


## start service
cd /opt/${NAME}
chown ${USER_NAME}:${GROUP_NAME} recordings
#su ${USER_NAME} -c "ulimit -n 65536 && ./galene -http :8000 -insecure -relay-only $@"
su ${USER_NAME} -c "ulimit -n 65536 && ./galene -http :8000 -insecure $@"


echo "stopping docker-entrypoint.sh"
echo "$(date)"
